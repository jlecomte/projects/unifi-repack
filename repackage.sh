#!/usr/bin/env bash

set -e
set -o pipefail

as_source=${1}
as_tmpdir="$(mktemp -d -p `pwd` 2>/dev/null)"

echo "info: extracting '${as_source}' to '${as_tmpdir}'..." >&2
dpkg-deb -R "${as_source}" ${as_tmpdir}

# build a '+mongodb3.6_all' version:
echo "info: modifying..." >&2
pushd ${as_tmpdir} >/dev/null

# https://repo.mongodb.org/apt/debian
sed "/^Version:/ s/$/+unlocked/"  -i DEBIAN/control
sed "/\bmongodb-server\b.*<<.*/d" -i DEBIAN/control

as_filename=unifi_$(grep -E "^Version: " DEBIAN/control | cut -d':' -f2 | xargs)_all
popd >/dev/null
  
echo "info: repackaging..." >&2
mkdir -p unifi
dpkg-deb -b ${as_tmpdir} unifi/${as_filename}.deb
 
